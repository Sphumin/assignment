package com.example.demo.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class DbConnectionFailed extends RuntimeException {

    public DbConnectionFailed(String message){
        super(message);
    }
}
