package com.example.demo.Service;

import org.springframework.stereotype.Service;

@Service
public class KafkaListener {
    @org.springframework.kafka.annotation.KafkaListener(topics = "my_topic", groupId ="my_group_id" )
    public void getMessage(String message)    {
        System.out.println(message);
    }
}
