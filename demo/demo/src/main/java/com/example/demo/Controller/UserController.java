package com.example.demo.Controller;

import com.example.demo.Exception.BaseException;
import com.example.demo.Exception.NotFoundId;
import com.example.demo.Exception.NotFoundUsername;
import com.example.demo.Exception.UserException;
import com.example.demo.Model.User;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/newUser")
    public ResponseEntity<User> create(@RequestBody User user) throws BaseException {
        if (user.getUsername() == null) {
            throw UserException.usernameNull();
        }
        userService.create(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping(value = "/userid")
    public ResponseEntity<List<User>> findAll() {
        List<User> list = userService.findAllUser();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "username/{username}")
    public User findByUsername(@PathVariable("username") String username)throws NotFoundUsername {
        Optional<User> userOptional = userService.findByUsername(username);
        try {
            return userOptional.get();
        }catch (RuntimeException e){
            throw new NotFoundUsername("User : "+ username +" not found");
        }
    }

    @GetMapping(value = "/id/{id}")
    public User findById(@PathVariable("id") Integer id)throws NotFoundId {
        Optional<User> userOptional = userService.findById(id);
        try {
            return userOptional.get();
        }catch (RuntimeException e){
            throw new NotFoundId("ID : " + id + "not found" );
        }
    }

    @PutMapping(value = "/id/{id}")
    public User update(@PathVariable("id") Integer id, @RequestBody User user)throws BaseException {
        if(user.getId() == null){
            throw UserException.idNull();
        }
        user.setId(id);
        return userService.update(user);
    }

    @PatchMapping(value = "/partialid/{id}")
    public ResponseEntity<User> updatePartial(@PathVariable("id") Integer id, @RequestBody User userUpdate)throws BaseException {
        Optional<User> userOptional = userService.findById(id);
        User user = userOptional.get();
        if (user.getUsername() == null) {
            throw UserException.usernameNull();
        }
        user.setUsername(userUpdate.getUsername());
        return new ResponseEntity<>(userUpdate, HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value = "/id/{id}")
    public String delete(@PathVariable("id") Integer id) throws NotFoundId {
        try{
            userService.deleteById(id);
            return "Deleted";
        }catch (RuntimeException e){
            throw new NotFoundId("ID : " + id + "not found" );
        }
    }
}
