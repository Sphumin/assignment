package com.example.demo.Controller;

import com.example.demo.Service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {
        @Autowired
        private KafkaService kafkaService;

        @PostMapping(value = "/topic/{message}")
        public String writeMessageToKafkaTopic(@PathVariable("message") final String message) {
            this.kafkaService.writeMessage(message);
            return "Sent topic:" + message;
        }
    }


