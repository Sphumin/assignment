package com.example.demo.Implementation;

import com.example.demo.Model.User;
import com.example.demo.Repository.UserRepository;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findById(Integer id) {

        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByUsername(String username) {

        return userRepository.findByUserName(username);
    }

    @Override
    public User create(User user) {

        return userRepository.save(user);
    }

    @Override
    public User update(User user) {

        return userRepository.save(user);
    }

    @Override
    public void deleteById(Integer id) {

        userRepository.deleteById(id);
    }

    @Override
    public User updatePartial(User user) {

        return userRepository.save(user);
    }
}
