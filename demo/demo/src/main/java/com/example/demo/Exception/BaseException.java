package com.example.demo.Exception;

public abstract class BaseException extends Exception{

    public BaseException(String code){
        super(code);
    }
}
