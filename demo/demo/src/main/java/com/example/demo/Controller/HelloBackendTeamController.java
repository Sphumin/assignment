package com.example.demo.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloBackendTeamController {

    ///feature-question-2
    @RequestMapping("/feature-question-2")
    public String HelloBackendTeam()
    {
        return "Hello Backend Team";
    }
}

