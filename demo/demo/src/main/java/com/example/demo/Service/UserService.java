package com.example.demo.Service;

import com.example.demo.Model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
   List<User> findAllUser();
   Optional<User> findById(Integer id);
   Optional<User> findByUsername(String username);
   User create(User user);
   User update(User user);
   void deleteById(Integer id);
   User updatePartial(User user);
}
