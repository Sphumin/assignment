package com.example.demo.Repository;

import com.example.demo.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Integer> {
    @Query(value = "SELECT * from userinfo WHERE id = ?1", nativeQuery = true)
    Optional<User> findById(Integer id);

    @Query(value = "SELECT * from userinfo WHERE username = ?1", nativeQuery = true)
    Optional<User> findByUserName(String username);
}

