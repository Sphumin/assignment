package com.example.demo.Exception;

public class UserException extends BaseException {

    public UserException(String code) {
        super("user." + code);
    }

    //user.newUser.username.null
    public static UserException usernameNull(){
        return new UserException("newUser.username.null");
    }

    public static UserException idNull(){
        return new UserException("newUser.id.null");
    }


}
