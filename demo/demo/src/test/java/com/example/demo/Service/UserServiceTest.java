package com.example.demo.Service;

import com.example.demo.Implementation.UserServiceImpl;
import com.example.demo.Model.User;
import com.example.demo.Repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    private User user;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setId(1);
        user.setUsername("A");
    }

    @Test
    void canFindAllUser() {
        //given
        given(userRepository.findAll()).willReturn(List.of(user));

        //when
        List<User> userList = userService.findAllUser();
        for(int i=0;i<userList.size();i++){
            System.out.println(userList.get(i).getId()+""+userList.get(i).getUsername());
        }

        //then
        assertThat(userList).isNotNull();
        assertThat(userList.size()).isEqualTo(1);
        for(int i =0;i<userList.size();i++){
            assertThat(userList.get(i)).isNotNull();
        }
    }

    @Test
    void canFindById() {
        //given
        given(userRepository.findById(1)).willReturn(Optional.of(user));

        //when
        User outUser = userService.findById(user.getId()).get();

        //then
        assertThat(outUser).isNotNull();
        assertThat(outUser.getId()).isEqualTo(1);
    }

    @Test
    void canFindByUsername() {
        //given
        given(userRepository.findByUserName("A")).willReturn(Optional.of(user));

        //when
        User outUser = userService.findByUsername(user.getUsername()).get();

        //then
        assertThat(outUser).isNotNull();
        assertThat(outUser.getUsername()).isEqualTo("A");
    }

    @Test
    void canCreate() {
        //given
        given(userRepository.save(user)).willReturn(user);

        //when
        User createUser = userService.create(user);

        //then
        assertThat(createUser).isNotNull();
    }

    @Test
    void update() {
        //given
        given(userRepository.save(user)).willReturn(user);
        user.setUsername("B");

        //when
        User updateUser = userService.update(user);

        //then
        assertThat(updateUser).isNotNull();
        assertThat(updateUser.getUsername()).isEqualTo("B");
    }

    @Test
    void deleteById() {
        //given
        Integer id = 2;
        willDoNothing().given(userRepository).deleteById(id);

        //when
        userService.deleteById(id);

        //then
        verify(userRepository,times(1)).deleteById(id);
    }

    @Test
    void updatePartial() {
        //given
        given(userRepository.save(user)).willReturn(user);
        user.setUsername("B");

        //when
        User updateUser = userService.update(user);

        //then
        assertThat(updateUser).isNotNull();
        assertThat(updateUser.getUsername()).isEqualTo("B");
    }
}