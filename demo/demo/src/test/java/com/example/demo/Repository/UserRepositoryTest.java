package com.example.demo.Repository;

import com.example.demo.Model.User;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.mockito.BDDMockito.given;

import org.assertj.core.api.Assertions;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserRepositoryTest {

    @Mock
    private UserRepository userRepository;

    private User user;

    @BeforeEach
    @Transactional
    void setUp() throws Exception {
        user = new User();
        user.setId(1);
        user.setUsername("A");
    }

    @Test
    @Order(1)
    @Rollback(value = false)
    void createRepo(){
        given(userRepository.save(user)).willReturn(user);

        User createUser = userRepository.save(user);

        Assertions.assertThat(createUser).isNotNull();
    }
    
    @Test
    @Order(2)
    void findById() {
        given(userRepository.findById(1)).willReturn(Optional.of(user));

        User findUser = userRepository.findById(1).get();

        Assertions.assertThat(findUser.getId()).isEqualTo(1);
    }

    @Test
    @Order(3)
    void findByUserName() {
        given(userRepository.findByUserName("A")).willReturn(Optional.of(user));

        User findUser = userRepository.findByUserName("A").get();

        Assertions.assertThat(findUser.getUsername()).isEqualTo("A");
    }
}