package com.example.demo.Controller;

import com.example.demo.Exception.NotFoundUsername;
import com.example.demo.Model.User;
import com.example.demo.Service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@AutoConfigureMockMvc
@WebMvcTest(controllers = UserController.class)
@ContextConfiguration(classes = {UserController.class})
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    UserService userService;
    @Autowired
    private ObjectMapper objectMapper;
    private User user;
    private User userTest;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setId(999);
        user.setUsername("A");
        userTest = new User();
        userTest.setId(1);
        userTest.setUsername("B");
    }
    @DisplayName("canCreateController")
    @Test
    void canCreateController()throws Exception {
        //given
        given(userService.create(any(User.class)))
                .willAnswer((invocation)-> invocation.getArgument(0));

        //when
        ResultActions response = mockMvc.perform(post("/user/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)));

        //then
        response.andDo(print()).
                andExpect(status().isCreated())
                .andExpect(jsonPath("$.username",is(user.getUsername())));
    }

    @DisplayName("canFindAll")
    @Test
    void canFindAll() throws Exception{
        //given
        List<User> userList = new ArrayList<>();
        given(userService.findAllUser())
                .willReturn(userList);

        //when
        Mockito.when(userService.findAllUser()).thenReturn(userList);
        ResultActions response = mockMvc.perform(get("/user/userid"));

        //then
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()",is(userList.size())));
    }

    @DisplayName("canFindByUsername")
    @Test
    void canFindByUsername()throws Exception {
        //given
        String username = "A";
        given(userService.findByUsername(username))
                .willReturn(Optional.of(user));

        //when
        ResultActions response = mockMvc.perform(get("/user/username/{username}",username));

        //then
        response.andDo(print())
                .andExpect(jsonPath("$.username",is(user.getUsername())));

    }

    @DisplayName("canFindById")
    @Test
    void canFindById() throws  Exception{
        //given
        Integer id = 999;
        given(userService.findById(id))
                .willReturn(Optional.of(user));

        //when
        ResultActions response = mockMvc.perform(get("/user/id/{id}",id));

        //then
        response.andDo(print())
                .andExpect(jsonPath("$.id",is(user.getId())));
    }

    @DisplayName("canUpdate")
    @Test
    void canUpdate() throws Exception {
        //given
        Integer id = 999;
        given(userService.findById(id))
                .willReturn(Optional.of(user));
        given(userService.update(any(User.class)))
                .willAnswer((invocation)-> invocation.getArgument(0));

        //when
        ResultActions response = mockMvc.perform(put("/user/id/{id}",id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userTest)));

        //then
        response.andDo(print())
                .andExpect(jsonPath("$.username",is(userTest.getUsername())));

    }

    @DisplayName("canUpdatePartial")
    @Test
    void canUpdatePartial() throws Exception{
        //given
        Integer id = 200;
        given(userService.findById(id))
                .willReturn(Optional.of(user));
        user.setUsername("AA");
        given(userService.update(any(User.class)))
                .willAnswer((invocation)-> invocation.getArgument(0));

        //when
        ResultActions response = mockMvc.perform(patch("/user/partialid/{id}",id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)));

        //then
        response.andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.username",is(user.getUsername())));
    }

    @DisplayName("canDelete")
    @Test
    void canDelete() throws Exception{
        //given
        Integer id = 999;
        willDoNothing().given(userService).deleteById(id);

        //when
        ResultActions response = mockMvc.perform(delete("/user/id/{id}",id));

        //then
        response.andDo(print())
                .andExpect(status().isOk());
    }
}